package fr.itk;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController(value="/")
public class Greetings {
	
	@RequestMapping(method=RequestMethod.GET, value="/hello")
	public String helloworld() {
		return "Hello world !";
	}

}
